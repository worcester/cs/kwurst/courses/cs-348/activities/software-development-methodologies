# Software Development Steps Activity

## Content Learning Objectives

By the end of this activity, participants will be able to...

- Describe the steps common to every software development methodology
- Put the steps for developing software into the correct order.

## Process Skill Goals

During the activity, students should make progress toward:

- 

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Steps involved in developing software

- Implementation
- Design
- Deployment
- Requirements Analysis
- Verification
- Maintenance

### Questions (12 min)

1. Describe each of the steps above. Specify what you think the output "product" of each step will be.

## Model 2: Order the steps

- Implementation
- Design
- Deployment
- Requirements Analysis
- Verification
- Maintenance

### Questions (6 min)

1. Put the steps above in order. Explain why.

---
Copyright © 2023 Karl R. Wurst. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
