# Software Development Methodologies Activity

## Content Learning Objectives

By the end of this activity, participants will be able to...

- Interpret a Gantt chart.
- Explain the high-level differences between Waterfall and Agile software
    development methodologies. 

## Process Skill Goals

During the activity, students should make progress toward:

- 

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

### Model 1: Waterfall Methodology

The *Waterfall* software development methodology is one where each step is
performed **in order**, **a single time**, and **each step is not started
until the previous step is completed**.

Below is an example *Gantt chart* for an invented software project.

```plantuml
@startuml
@startgantt
hide footbox
projectscale monthly
title Example Project
Project starts 2023-10-01
[Requirements Analysis] lasts 91 days
then [Design] lasts 61 days
then [Implementation] lasts 152 days
then [Verification] lasts 61 days
then [Deployment] lasts 30 days
footer Maintenance is not included as it is a long-term, ongoing process.

@endgantt
@enduml
```


### Questions (15 min)

1. How many months long is development for this example project?
2. How many months long are each of the steps?

    - Requirements Analysis
    - Design
    - Implementation
    - Verification
    - Deployment

3. Why do you think Requirements Analysis and Design are take such a long
   time in a Waterfall project?
4. Assuming that Requirements Analysis involves interviewing the customer
   on what they want, and that the customer will not see the software until Deployment is complete, how long are the developers working with no contact with the customer?
5. Could you argue for customer contact earlier than the end of Deployment?
   In which step, and why?
6. What sorts of problems do you see occurring in a project that uses the
   Waterfall methodology, and why?

### Model 2: Agile Methodology

One of the features of the  *Agile* software development methodology is that
**the cycle of steps is performed repeatedly**, in **short *increments***.

```plantuml
@startuml
@startgantt
hide footbox
projectscale weekly
header Numbers represent week numbers from beginning of the year
title Example Project
Project starts 2023-10-02
[Increment 1] lasts 14 days
then [Increment 2] lasts 14 days
then [Increment 3] lasts 14 days
then [Increment 4] lasts 14 days
then [Increment 5] lasts 14 days
then [Increment 6] lasts 14 days
then [Increment 7] lasts 14 days
then [Increment 8] lasts 14 days
then [Increment 9] lasts 14 days
footer Increments continue until project is completed
@endgantt
@enduml
```

```plantuml
@startuml
state Increment {
    state "Requirements Analysis" as ra
    [*] --> ra
    ra -> Design
    Design -> Implementation
    Implementation -> Verification
    Verification -> Deployment
    Deployment -->[*]
}
@enduml
```

### Questions (20 min)

1. How long is an increment in this project?
2. How much time is spent each of the steps within an increment?
3. Assuming that Requirements Analysis involves interviewing the customer
   on what they want, how often are the developers in contact with the
   customer?
4. How often does the customer see a new version of the software?
5. How often do developers get feedback about whether the project meets the
   customer's requirements?
6. How often does the customer have a chance to update the requirements
   based on seeing the software?
7. How does using an Agile methodology change the scope of each of the steps?
   In other words, how much can be specified, designed, implemented, tested, and deployed at a time?
8. How do you think the customer and developers will decide what to work on
   in each increment?
9. Why do you think an Agile methodology works for developing software,
   but designing and building a bridge requires a process like Waterfall?

---
Copyright © 2024 Karl R. Wurst. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
